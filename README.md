# Steps for Setting up ArrowAI Custom Bot

## Create a Project in Google Action

Create a new Project with Custom and Blank Option Selected.

## Download the Gaction CLI 

* Download the Gaction CLI from the Following URL
* Do a **Gactions login** to login

## Run the Fulfillment

* Go to testprojfulfil folder and run the code.
* When the code is running find the URL (in case of localhost, use ngrok)


## Deploy the code

* Go to the folder of testproj and make changes in webhook and settings
* Do **gactions deploy preview** to test in simulator
* Do **gactions deploy prod** to deploy in Prod
* Test in simulator