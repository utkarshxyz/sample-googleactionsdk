const express = require('express')
const bodyParser = require('body-parser')

const expressApp = express().use(bodyParser.json())

expressApp.post('/', (req, res) => {
    console.log(req.body);
    res.json({
          "session": {
            "id": "session_id",
            "params": {}
          },
          "prompt": {
            "override": false,
            "firstSimple": {
              "speech": "This is the first simple response.",
              "text": "This is the 1st simple response."
            },
            "lastSimple": {
              "speech": "This is the last simple response.",
              "text": "This is the last simple response."
            }
          }
        
      })
  })

expressApp.listen(3000)